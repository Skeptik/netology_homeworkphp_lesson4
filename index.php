<?php 
$cityName = 'London';
$appid = 'appid=6ce8dd59cdaa7c4c749a8c6bf3182145';
$url = "http://api.openweathermap.org/data/2.5/weather?q=$cityName&$appid";
if (file_exists(__DIR__."/json/json.txt")){
	$check = fileTime();
	if ($check < 60){
		$file = fopen(__DIR__."/json/json.txt", 'r');
		$arrWeather= json_decode(fgets($file), true);
	}else{
		addFile();
	}	
}else{
	addFile();
}
fclose($file);
$temperature = $arrWeather['main']['temp']; // Температура
$humidity = $arrWeather['main']['humidity']; // Влажность 
$pressure = $arrWeather['main']['pressure']; // Давление
$windSpeed = $arrWeather['wind']['speed']; // Скорость ветра
$weatherPatterns = $arrWeather['weather'][0]['description']; //Погодные условия
$icon = $arrWeather['weather'][0]['icon'];

function fileTime (){
	$fileTime = filemtime(__DIR__."/json/json.txt");
	$thisTime = microtime(true);
	$result = ($thisTime - $fileTime)/60;
    return round($result, 0, PHP_ROUND_HALF_DOWN);
}

function addFile(){
	global $url, $file, $arrWeather;
	$data = file_get_contents($url);
	$file = fopen (__DIR__."/json/json.txt", "w");
	fputs($file, $data);
	$arrWeather= json_decode($data, true);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Weather</title>
</head>
<body>
	<h1>Погода <?= $cityName ?></h1>
	<div>Температура <?= $temperature ?></div>
	<div>Влажность <?= $humidity ?></div>
	<div>Давление <?= $pressure ?></div>
	<div>Скорость ветра <?= $windSpeed ?></div>
	<div>Погодные условия <?= $weatherPatterns ?></div>
	<img src='http://openweathermap.org/img/w/<?=$icon?>.png' weight='130' height='130'>
</body>
</html>